/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    GameScreen screen = LOGO;
    InitAudioDevice();
    
    //texturas
    Texture2D logo = LoadTexture("Resources/logo.png");
    Texture2D background = LoadTexture("Resources/background.png");
    Texture2D player_text = LoadTexture("Resources/player.png");
    Texture2D enemy_text = LoadTexture("Resources/enemy.png");
    Texture2D ball_text = LoadTexture("Resources/ball.png");
    
    //sonidos
    Sound explosion = LoadSound("Resources/sound.wav");
    Music gameMusic = LoadMusicStream("Resources/musicpong.mp3");
    SetMusicVolume(gameMusic, 0.08f);
    SetSoundVolume(explosion, 0.5f);
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    //logo
    float alpha = 0.0f;
    bool FadeIn = true;
    
    /*
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    */
    
    //gameplay
    Color ballColor = { 1, 183, 104, 255};
    Color playerColor = YELLOW;
    Color playerFillColor = { 190, 139, 0, 255};
    Color enemyColor = RED;
    Color enemyFillColor = {102, 28, 28, 255};
    
    float topMargin = 50;
    float bgMargin = 8;
    float fieldMargin = 5;
    float counterMargin = 35;
    float lifeMargin = bgMargin;
    
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin};
    Rectangle fieldRec = { bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2, bgRec.height - bgMargin*2};
    
    Rectangle playerBgRec = { 0, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle playerFillRec = { playerBgRec.x + lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec;
    
    Rectangle enemyBgRec = {screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle enemyFillRec = { enemyBgRec.x + lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec;
    
    Rectangle player = { fieldRec.x + fieldMargin, fieldRec.y + fieldRec.height/2 - 40, 20, 80};
    int playerSpeedY = 4;
    
    Rectangle enemy = { fieldRec.x + fieldRec.width - fieldMargin - 20, fieldRec.y + fieldRec.height/2 - 40, 20, 80};
    int enemySpeedY = 4;
    float AImargin = enemy.height/4;
    
    Vector2 ballPosition = {fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2 };
    Vector2 ballSpeed = { GetRandomValue (4,5), GetRandomValue(5,6) };
    
    
    int ballRadius = 20;
    
    int playerLife = 5;
    int enemyLife = 5;
    
    float widthDamage = playerLifeRec.width/playerLife;
    
    //titulo
    
    Font PacManFont = LoadFont("Resources/font.TTF");
    char titleText[16] = "Pac-PongMan";
    char startText[16] = "Press enter";
    int titleSize = 120;
    Vector2 titlePos = {screenWidth/2 - MeasureTextEx(PacManFont, titleText, titleSize, 0).x/2, -100};
    bool blink = false;
    
    //dentro juego
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    bool pause = false;
    
    
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if(FadeIn)
                {
                    alpha += 1.0f/90;
                    
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                         
                        framesCounter ++;
                        if(framesCounter % 120 == 0)
                        {
                           framesCounter = 0;
                           FadeIn = false; 
                        }
                             
                    }
                }
                else
                {
                    alpha -= 1.0f/90;
                    
                    if(alpha <= 0.0f)
                    {
                        framesCounter = 0;
                        screen = TITLE;
                    } 
                }
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if(titlePos.y < 100)
                {
                    titlePos.y += 2;
                    
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        titlePos.y =100;
                    }
                }
                else
                {
                    titlePos.y = 100;
                    framesCounter ++;
                    if (framesCounter %8 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        framesCounter = 0;
                        PlayMusicStream (gameMusic);
                        screen = GAMEPLAY;
                    }
                }
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
                UpdateMusicStream(gameMusic);
                
                if(!pause)
                   {
                ballPosition.x += ballSpeed.x;
                ballPosition.y += ballSpeed.y;
                
                if(IsKeyDown(KEY_UP)) player.y -= playerSpeedY;
                if(IsKeyDown(KEY_DOWN)) player.y += playerSpeedY;
              
                if(player.y < fieldRec.y) player.y = fieldRec.y;
                if(player.y > fieldRec.y + fieldRec.height - player.height) player.y = fieldRec.y + fieldRec.height - player.height;
             
             
                if((ballPosition.x > fieldRec.x + fieldRec.width/2) && ballSpeed.x > 0)
                {
                   if( ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                   if( ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                   
                }
                if(enemy.y < fieldRec.y) enemy.y = fieldRec.y;
                if(enemy.y > fieldRec.y + fieldRec.height - enemy.height) enemy.y = fieldRec.y + fieldRec.height - enemy.height;
                if(CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0)
                {
                    ballSpeed.x *= -1.01f;
                }
                
                    if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0)
                {
                    ballSpeed.x *= -1.01f;
                }
                
                if((ballPosition.y - ballRadius < fieldRec.y) && ballSpeed.y < 0)
                {
                    ballSpeed.y *= -1;
                }
                if((ballPosition.y + ballRadius > fieldRec.y + fieldRec.height) && ballSpeed.y > 0)
                {
                    ballSpeed.y *= -1;
                }
                if((ballPosition.x - ballRadius < fieldRec.x) && ballSpeed.x < 0)
                {
                    PlaySound(explosion);
                    ballSpeed.x *= -1;
                    playerLife --;
                    playerLifeRec.width -= widthDamage;
                }
                
                if((ballPosition.x + ballRadius > fieldRec.x + fieldRec.width) && ballSpeed.x > 0)
                {
                    PlaySound(explosion);
                    ballSpeed.x *= -1;
                    enemyLife --;
                    enemyLifeRec.width -= widthDamage;
                    enemyLifeRec.x += widthDamage;
                }
               
                 framesCounter++;
                 if(framesCounter % 60 == 0)
                 {
                     framesCounter = 0;
                     secondsCounter --;
                 }
                
            
                
                     if(secondsCounter <= 0)
                      {
                          if(playerLife < enemyLife) gameResult = 0;
                          else if(playerLife > enemyLife) gameResult = 1;
                          else gameResult = 2;
                      }
                      else if (playerLife <= 0) gameResult = 0;
                      else if (enemyLife <= 0) gameResult = 1;
                
                     
                      
                      if(gameResult != -1)
                      {
                          StopMusicStream(gameMusic);
                          screen = ENDING;
                      }
                  
                
                    
                }
                if(IsKeyPressed(KEY_P))
                {
                     pause = !pause;
                     if(pause) PauseMusicStream(gameMusic);
                     else ResumeMusicStream(gameMusic);
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if(IsKeyPressed(KEY_ENTER))
                {
                    ballPosition = (Vector2) {fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2 };
                    ballSpeed = (Vector2) { GetRandomValue (4,5), GetRandomValue(5,6) };

                    if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                    if(GetRandomValue(0,1)) ballSpeed.y *= -1;

                    player.y = fieldRec.y + fieldRec.height/2 - 40;
                    enemy.y = player.y;

                    playerLife = 5;
                    enemyLife = 5;

                    playerLifeRec = playerFillRec;
                    enemyLifeRec = enemyFillRec;

                    secondsCounter = 99;
                    framesCounter = 0;

                    pause = false;

                    gameResult = -1;

                    PlayMusicStream(gameMusic);
                    screen = GAMEPLAY;
                }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, Fade(WHITE, alpha));
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLUE);
                    DrawTextEx(PacManFont, titleText, titlePos, titleSize, 0 , YELLOW);
                    
                    
                    if (blink)DrawText(startText, screenWidth/2 - MeasureText(startText,20)/2, screenHeight - 100, 20, WHITE);
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    DrawRectangleRec(bgRec, BLACK);
                    DrawTexture(background, 0, 0, WHITE);
                    DrawLineEx((Vector2){bgRec.x + bgRec.width/2, bgRec.y},(Vector2){bgRec.x + bgRec.width/2, bgRec.y + bgRec.height}, bgMargin, BLACK);
                    DrawTexture(player_text, player.x, player.y, WHITE);
                    DrawTexture(enemy_text, enemy.x, enemy.y, WHITE);
                    //DrawCircleV(ballPosition, ballRadius, ballColor);
                    DrawTextureV(ball_text, (Vector2){ballPosition.x - 20, ballPosition.y - 20}, WHITE);
                    
                    
                    
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, playerFillColor);
                    DrawRectangleRec(playerLifeRec, playerColor);
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, enemyFillColor);
                    DrawRectangleRec(enemyLifeRec, enemyColor);
                    
                 
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, topMargin - 42, 40, ballColor);
                    
                    if (pause)
                    {
                        DrawRectangle(0,0,screenWidth,screenHeight,Fade(BLACK, 0.8f));
                        DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE",30)/2, screenHeight/2- 15,30, BLUE);
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if(gameResult == 0) DrawText("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE",40)/2, 150, 40, RED);
                    else if (gameResult == 1) DrawText("YOU WIN", screenWidth/2 - MeasureText("YOU WIN",40)/2, 150, 40, GREEN);
                    else if (gameResult == 2) DrawText("DRAW", screenWidth/2 - MeasureText("DRAW",40)/2, 150, 40, YELLOW); 

                    DrawText("[ENTER] Retry", screenWidth/2 - MeasureText("[ENTER] Retry",20)/2, screenHeight - 100, 20, BLACK); 
                    DrawText("[ESC] Exit", screenWidth/2 - MeasureText("[ESC] Exit",20)/2, screenHeight - 75 , 20, BLACK);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo);
    UnloadFont(PacManFont);
    UnloadTexture(background);
    UnloadTexture(player_text);
    UnloadTexture(enemy_text);
    UnloadTexture(ball_text);
    UnloadSound(explosion);
    UnloadMusicStream(gameMusic);
    
    CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}